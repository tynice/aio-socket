/*
 *    Copyright 2019 The aio-socket Project
 *
 *    The aio-socket Project Licenses this file to you under the Apache License,
 *    Version 2.0 (the "License"); you may not use this file except in compliance
 *    with the License. You may obtain a copy of the License at:
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package cn.starboot.http.server.banner;

/**
 * Created by DELL(mxd) on 2022/12/30 16:00
 */
public class HttpBanner {

	public static final String BANNER = "            _                                               __              __       __    __  __                                           \n" +
			"  ____ _   (_)  ____              _____   ____     _____   / /__   ___     / /_     / /_  / /_/ /_____       ________  ______   _____  _____\n" +
			" / __ `/  / /  / __ \\   ______   / ___/  / __ \\   / ___/  / //_/  / _ \\   / __/    / __ \\/ __/ __/ __ \\     / ___/ _ \\/ ___/ | / / _ \\/ ___/\n" +
			"/ /_/ /  / /  / /_/ /  /_____/  (__  )  / /_/ /  / /__   / ,<    /  __/  / /_     / / / / /_/ /_/ /_/ /    (__  )  __/ /   | |/ /  __/ /    \n" +
			"\\__,_/  /_/   \\____/           /____/   \\____/   \\___/  /_/|_|   \\___/   \\__/    /_/ /_/\\__/\\__/ .___/    /____/\\___/_/    |___/\\___/_/     \n" +
			"                                                                                              /_/                                           ";

}
